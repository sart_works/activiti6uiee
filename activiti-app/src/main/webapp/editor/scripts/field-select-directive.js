angular.module('activitiModeler').
    directive('fieldSelect', [
        '$rootScope',
        '$http',
        '$templateCache',
        '$timeout',
        //'ProcessScopeService',
        //function(e, t, i, o, n) {
    
        function($rootScope, t, i, $timeout) {
            var directive = {};
            return directive.restrict = 'A',
                directive.templateUrl = 'views/includes/select-field.html',
                directive.replace = !0,
                directive.scope = {
                    selectedField: '=fieldSelect',
                    availableFormFields: '=fields',
                    includeType: '=includeType',
                    expandRelative: '=expandRelative',
                    selectTitle: '=selectTitle',
                    filterDisplayFields: '=filterDisplayFields',
                    fieldTypeFilter: '=fieldTypeFilter',
                    variablesType: '@variablesType',
                    editorType: '@editorType',
                    stepId: '=stepId',
                    allSteps: '=allSteps',
                    processModelId: '=processModelId'
                },
                directive.controller = [
                    '$scope',
                    //'$element',
                    function($scope) {
                        $scope.selectField = function(field) {
                                $scope.selectedField = field
                            },
                            
                            $scope.refreshFields = function() {
                                var fields = [];
                                var fieldTypes = $scope.fieldTypeFilter;
                                if (fieldTypes && fieldTypes.length > 0) {
//                                    for (var r = 0; fieldTypes.length > r; r++) {
//                                        var field = {id: fieldTypes[r],
//                                            name: fieldTypes[r]
//                                            };   
                                        var field = {id: "customer_id",
                                            name: "Выбранный клиент",
                                            type: "customers"
                                            };
                                        
                                        fields.push(field);
//                                    }    
                                }
                                $scope.formFields = fields
                            }
                        
                            $scope.refreshFields_bak = function() {
                                var t = [];
                                if ($scope.variablesType && 'rest' == $scope.variablesType) {
                                    if ($scope.variablesType && 'rest' === $scope.variablesType) {
                                        var i = [],
                                            o = n.getVariablesForStep($scope.allSteps, $scope.stepId, $scope.fieldTypeFilter);
                                        if (o && o.length > 0)
                                            for (var r = 0; o.length > r; r++) {
                                                var s = {
                                                    id: o[r].processVariableName,
                                                    name: o[r].processVariableName
                                                };
                                                $scope.includeType && (s.type = o[r].processVariableType),
                                                    $scope.editorType && 'form' === $scope.editorType && (s.responseVariable = !0),
                                                    i.push(s)
                                            }
                                        t = i
                                    }
                                } else {
                                    var a = [];
                                    if (a = $scope.availableFormFields ? $scope.availableFormFields : 
                                        n.getFormFieldsForStep($scope.allSteps, $scope.stepId, $scope.fieldTypeFilter), 
                                            a && a.length > 0)
                                        for (var r = 0; a.length > r; r++) {
                                            var l = a[r];
                                            if (!$scope.filterDisplayFields || 'dynamic-table' !== l.type) {
                                                var d = {
                                                    id: l.id,
                                                    name: l.name
                                                };
                                                $scope.includeType && (d.type = l.type),
                                                    'dynamic-table' === l.type ? d.columnDefinitions = l.columnDefinitions : 
                                                    'amount' === l.type ? (d.currency = l.currency || '$', d.enableFractions = l.enableFractions) : 
                                                    'hyperlink' === l.type && (d.hyperlinkUrl = l.hyperlinkUrl, d.displayText = l.displayText),
                                                    t.push(d)
                                            }
                                        }
                                }
                                t.sort(function($scope, t) {
                                        return $scope.name.toLowerCase() > t.name.toLowerCase() ? 1 : -1
                                    }),
                                    $scope.formFields = t
                            },
                        
                            $scope.fixPosition = function(t) {
                                //$scope.expandRelative && 
                                $timeout(function() {
                                    var e = angular.element(t.target);
                                    e.next().css('position', 'relative'),
                                        e.parent().on('hide.bs.dropdown', function() {
                                            e.next().css('position', 'absolute'),
                                                e.parent().off('hide.bs.dropdown')
                                        })
                                }, 0);
                            };
                            $scope.refreshFields();
                    }
                ],
                directive.link = function($scope, $timeout, i) {
                    i.filterType && ($scope.filterType = i.filterType, $timeout(function() {
                        $scope.refreshFields()
                    }))
                },
                directive
        }
    ]);