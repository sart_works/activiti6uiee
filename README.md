# README #

This project is forked from [Activiti6UI](https://github.com/Activiti/Activiti/tree/activiti6) branch.
The main purpose of the project is creating enhanced version of the Activiti6UI module.

**Everyone is welcome to participate!**

### What is this repository for? ###
The basic Activiti6UI is not fully functional. It contains some bugs and if you plan to use this module in production you have to deal with these bugs. 
Also there is a big probability that you will stuck in the situation when the basic functionality is not enough for you. For example, the read-only form control is not working with script properties. You even cannot use custom form outcomes (custom action buttons such as Accept/Reject)...

### What has changed in this version ###
All bug fixes and enhancements are in this wiki section: 
[Bug fixes and New features](https://bitbucket.org/sart_works/activiti6uiee/wiki/Bug%20fixes%20and%20New%20features)

### Versions ###
The module is based on Activiti 6.0.0beta2 version and all of the customisation is made only inside this module. It means that module should work with any new Activiti version starting from 6.0

### How do I get set up? ###
Basically you only need to save activiti-app.war file into your tomcat web folder.
If you have original activiti-app installed, you'd better backup your version first.

### Configuration ###
The same as original 

### How to get help? ###
Write me directly or post your question here.

### Contribution guidelines ###
Post your pull requests and/or write me about your ideas.